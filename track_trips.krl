ruleset track_trips {
  meta {
    name "track_trips2"
    description <<
      Playing with track_trips2
    >>
    author "Sean George"
    logging on
    shares long_trip
  }

  global {
    long_trip = 50
  }

  rule process_trip is active {
    select when car new_trip mileage re#(.*)# setting(mileage);
    send_directive("trip") with
      trip_length = mileage


    fired {
        raise explicit event "trip_processed"
          attributes event:attrs().klog("Event attributes: ")
    }
  }

  rule find_long_trips is active {
    select when explicit trip_processed
    pre {
      mileage = event:attr("mileage").as("Number").klog("mileage is: ") 
      longest = long_trip.as("Number").klog("longest is:")
    }
    fired {
      raise explicit event "found_long_trip"
        attributes event:attrs()
      if (mileage > longest)

    }
  }

}