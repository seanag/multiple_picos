ruleset trip_store {
  meta {
    name "trip_store"
    description <<
      Playing with trip_store
    >>
    author "Sean George"
    logging on
    provides trips, long_trips, short_trips
    shares trips, long_trips, short_trips
  }

  global {
    clear_trip = {"-1":"-1"}

    trips = function() {
      ent:trips.defaultsTo({})
    }

    long_trips = function() {
      ent:long_trips.defaultsTo({})
    }

    short_trips = function () {
      tripMileages = ent:trips.klog("tripMileages is : ");
      longTripMileages = ent:long_trips.klog("longTripmileages is: ");
      trips = tripMileages.values().filter(function(x){x.as("Number") < 50 && x.as("Number") != -1})
    }
  }

  rule collect_trips is active {
    select when explicit trip_processed
    pre {
      mileage = event:attr("mileage").as("Number").klog("mileage is: ")
      timestamp = time:now()
    }
    always {
      ent:trips := ent:trips.defaultsTo({});
      ent:trips{[timestamp]} := mileage
    }
   
  }

  rule collect_long_trips is active {
    select when explicit found_long_trip
    pre {
      mileage = event:attr("mileage").as("Number").klog("mileage is: ")
      timestamp = time:now()
    }
    always {
      ent:long_trips := ent:long_trips.defaultsTo({});
      ent:long_trips{[timestamp]} := mileage
    }
  }

  rule clear_names is active {
    select when car trip_reset
    always {
      ent:trips := {};
      ent:long_trips := {}

    }
  }

  rule send_report {
      select when reports get_report 
      pre {
        originator = event:attr("originator").klog("in send_report witih originator ")
        id = event:attr("id").klog("id is ")
        trips = trips()
        test = event:attrs().klog("events in get_report ")
        name = event:attr("name")
      }
      if 1 == 1 then
        event:send(
        {
          "eci": originator,
          "eid": "send-report",
          "domain": "reports",
          "type": "receive_report",
          "attrs": {
              "trips" : trips,
              "id" : id,
              "name" : name
          } 
        }) 
    }

 rule auto_accept {
    select when wrangler inbound_pending_subscription_added
    pre{
      attributes = event:attrs().klog("subcription :")
      }
    always{
      raise wrangler event "pending_subscription_approval"
          attributes attributes       
    }
  } 
}





















