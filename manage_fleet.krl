ruleset manage_fleet {
  meta {
    use module io.picolabs.pico alias wrangler
    use module Subscriptions
    shares __testing, nameFromID, vehicles, sendTripsGet, getTrips, getReports
    provides nameFromID, vehicles
  }
  global {
    __testing = { "queries": [ { "name": "__testing" },
                                {"name": "vehicles"}, 
                                {"name":  "sendTripsGet", "args" : ["eci"]},
                                {"name":  "getTrips"},
                                {"name":  "getReports"}
                             ],
                  "events": [  
                    { "domain": "initiate", "type": "subscription", "attrs": ["eci", "name"] },
                    { "domain": "car", "type": "new_vehicle", "attrs": ["car_id"]},
                    { "domain": "clear", "type": "fleet", "attrs": []},
                    { "domain": "clear", "type": "reports", "attrs": []},
                    { "domain": "car", "type": "unneeded_vehicle", "attrs": ["id"]},
                    { "domain": "explicit", "type": "get_reports", "attrs": []},
                    { "domain": "reports", "type": "initialize_report", "attrs": []}
                  ] 
                }

      nameFromID = function(car_id) {
        "Car " + car_id
         
      }

      vehicles = function() {
        Subscriptions:getSubscriptions().filter(function(v,k){
          v{"attributes"}{"subscriber_role"} == "vehicle"
        })
      }

      getTrips = function() {
        subscriptions = vehicles().klog("subscriptions are ");
        allTrips = {};
        subscriptions = subscriptions.map(function(v,k){
          v = v{"attributes"}{"subscriber_eci"}
        }).klog("subscriptions after map ");
        allTrips = subscriptions.map(function(v,k){
          v = sendTripsGet(v)
        });

        finalMap = {};
        id = time:now();
        finalMap{[id]} = {};
        temp = {};
        temp{"vehicles"} = vehicles().keys().length;
        temp{"responding"} = allTrips.keys().length;
        temp{"trips"} = allTrips;
        finalMap{[id]} = temp
      }

      sendTripsGet = function(eci) {
        host = meta:host.klog("Host is ");
        url = host+"/sky/cloud/"+eci+"/trip_store/trips";
        response = http:get(url,{}).klog("Response is ");
        content = response{"content"}.klog("Content is ")
      }

      getReports = function() {
        ent:reports.defaultsTo({});
        size = ent:reports.keys().length;
        allKeys = ent:reports.keys();
        reports = (size > 4) => getFiveReports(allKeys) | ent:reports.defaultsTo({})
      }

      getFiveReports = function(allKeys) {
        allKeys.klog("allkeys is ");
        size = allKeys.length;
        keys = [];
        keys[0] = allKeys[size - 1].klog("In getFiveReports 1");
        keys[1] = allKeys[size - 2];
        keys[2] = allKeys[size - 3];
        keys[3] = allKeys[size - 4];
        keys[4] = allKeys[size - 5];
        reports = {};
        reports{[keys[0]]} = ent:reports{keys[0]};
        reports{[keys[1]]} = ent:reports{keys[1]};
        reports{[keys[2]]} = ent:reports{keys[2]};
        reports{[keys[3]]} = ent:reports{keys[3]};
        reports{[keys[4]]} = ent:reports{keys[4]};
        reports

      }
  }

  rule create_vehicle {
    select when car new_vehicle 
    
    pre {
      car_id = event:attr("car_id")
      exists = ent:fleet >< car_id
    }
    if not exists
    then
      noop()
    fired {
      raise pico event "new_child_request"
        attributes { "dname": nameFromID(car_id),
                     "color": "#FF69B4",
                     "car_id": car_id
                    }
    } else {
      car_id.klog("car_id already exists ")
    }
  }

  rule initate_subscription {
    select when explicit subscription
    pre {
      eci = event:attr("eci").klog("initate_subscription received eci ")
      name = event:attr("name").klog("name is ")
    }
    if eci then
      event:send(
      { "eci": wrangler:myself().eci, "eid": "subscription",
        "domain": "wrangler", "type": "subscription",
        "attrs": { "name": name,
                   "name_space": "fleet",
                   "my_role": "fleet",
                   "subscriber_role": "vehicle",
                   "channel_type": "subscription",
                   "subscriber_eci": eci } } )
    fired {
      eci.klog("Sent subscription to ")
    } else {
      eci.klog("Subsciption failed ")
    }

  }

  rule pico_child_initialized {
    select when pico child_initialized
    pre {
      the_car = event:attr("new_child")
      car_id = event:attr("rs_attrs"){"car_id"}
      name = nameFromID(car_id)
    }
    if car_id.klog("found car_id")
    then
      event:send(
         { "eci": the_car.eci, "eid": "install-ruleset",
           "domain": "pico", "type": "new_ruleset",
           "attrs": { "rid": "Subscriptions" } } )
      event:send(
         { "eci": the_car.eci, "eid": "install-ruleset",
           "domain": "pico", "type": "new_ruleset",
           "attrs": { "rid": "trip_store"} } )
      event:send(
         { "eci": the_car.eci, "eid": "install-ruleset",
           "domain": "pico", "type": "new_ruleset",
           "attrs": { "rid": "track_trips" } } )
    fired {
      ent:fleet := ent:fleet.defaultsTo({});
      ent:fleet{[name]} := the_car.klog("Adding car");
      raise explicit event "subscription"
        with eci = the_car.eci
        and name = name
      
    }
  }

  rule delete_vehicle {
    select when car unneeded_vehicle
    pre {
      name = nameFromID(event:attr("id")).klog("name in car:unneeded_vehicle is ")
      exists = ent:fleet >< name
      eci = meta:eci
    }
    if exists then
        send_directive("car deleted")
          with car_name = name
    fired {
      raise wrangler event "subscription_cancellation"
        with subscription_name = "fleet:" + name;
      raise pico event "delete_child_request"
        attributes ent:fleet{name};
      ent:fleet{[name]} := null
    }

  }

  rule initialize_report {
    select when reports initialize_report
    pre{
      vehicles = vehicles()
      id = (time:now()).klog("id of new report is ")
      numberOfVehicles =  Subscriptions:getSubscriptions().filter(function(v,k){
          v{"attributes"}{"subscriber_role"} == "vehicle"
        }).keys().length.klog("number of vehicles in new report")
    }

    always {
      ent:reports := ent:reports.defaultsTo({});
      temp = {};
      temp{["vehicles"]} = numberOfVehicles;
      temp{["responding"]} = 0;
      temp{["trips"]} = {};
      ent:reports{[id]} := temp;
      ent:reports.klog("This is reports after initialization");
      raise explicit event "get_reports"
        attributes {"id":id} 
      }
  

  }

  rule get_reports {
    select when explicit get_reports
    foreach vehicles() setting(vehicle)
    pre {
      vehicleAttrs = vehicle{"attributes"}
      originator = meta:eci.klog("ECI is ")
      id = event:attr("id").klog("with id ")
      name = vehicleAttrs{"subscription_name"}
      }

    if vehicleAttrs{"subscriber_role"} == "vehicle" then 
      event:send(
        {
          "eci": vehicleAttrs{"outbound_eci"},
          "eid": "get-report",
          "domain": "reports",
          "type": "get_report",
          "attrs": {
            "originator" : originator,
            "id" : id,
            "name" : name
          } 
        })
  }

  rule receive_report {
    select when reports receive_report
    pre {
      id = event:attr("id").klog("id in receive_report is ")
      test = event:attrs().klog("event attrs in receive_report")
      trips = event:attr("trips")
      name = event:attr("name").klog("name in receive_report is ")
    }
    always {
      ent:reports := ent:reports.defaultsTo({});
      temp = ent:reports{[id]}.klog("temp in receive_report is ");
      reported = temp{"responding"}.as("Number") + 1;
      temp{"responding"} = reported;
      tripsMap = temp{"trips"};
      tripsMap{[name]} = trips;
      temp{"trips"} = tripsMap;
      ent:reports{[id]} := temp
    }

  }

  rule clear_fleet {
    select when clear fleet
    always {
       ent:fleet := {}
    }
  }
  rule clear_reports {
    select when clear reports
    always {
       ent:reports := {}
    }
  }


}